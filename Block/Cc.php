<?php
namespace Tezus\Asaas\Block\Form;

class Cc extends \Magento\Payment\Block\Form\Cc
{
    /**
     * @var string
     */
    protected $_template = 'Tezus_Asaas::form/cc.phtml';
}