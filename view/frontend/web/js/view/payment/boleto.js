define(
  [
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
  ],
  function (
    Component,
    rendererList
  ) {
    'use strict';
    rendererList.push(
      {
        type: 'boleto',
        component: 'Tezus_Asaas/js/view/payment/method-renderer/boleto-method'
      }
    );
    return Component.extend({});
  }
);