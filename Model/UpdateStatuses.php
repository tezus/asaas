<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tezus\Asaas\Model;

use Tezus\Asaas\Api\UpdateStatusesInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Defines the implementaiton class of the calculator service contract.
 */
class UpdateStatuses implements UpdateStatusesInterface {
  protected $orderFactory;
  public function __construct(
    OrderRepositoryInterface $orderRepository,
    \Magento\Sales\Model\OrderFactory $orderFactory

  ) {
    $this->orderFactory = $orderFactory;
    $this->orderRepository = $orderRepository;
  }

  /** 
   * Post Company.
   *
   * @api
   * @param  mixed $event 
   * @param  mixed $payment
   * @return  mixed 
   */
  public function doUpdate($event, $payment) {

    $paymentobj = (array) $payment;
    $orderId =  $this->orderFactory->create()->loadByIncrementId($paymentobj['externalReference']);
    if ($event == "PAYMENT_CONFIRMED" OR $event == "PAYMENT_RECEIVED") {
      $order = $this->orderRepository->get($orderId->getId());
      if($orderId->getId()){
      
      $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
      $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);

      

        $this->orderRepository->save($order);
  
     
    }else{
      print_r(json_encode('Order Id not found'));
       return http_response_code(400);
    }
  }
    elseif($event == "PAYMENT_OVERDUE" OR 
    $event == "PAYMENT_DELETED" OR 
    $event == "PAYMENT_RESTORED" OR
    $event == "PAYMENT_REFUNDED" OR 
    $event == "PAYMENT_AWAITING_CHARGEBACK_REVERSAL"){

      $order = $this->orderRepository->get($orderId->getId());
      if($orderId->getId()){

      
      $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
      $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
      
     
        $this->orderRepository->save($order);
     
        $event = $e->getMessage();
        return http_response_code(200);
      }
      else{
        print_r(json_encode('Order Id not found'));
        return http_response_code(400);
      }
    }
   
  }
}
