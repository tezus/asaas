<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Tezus\Asaas\Model\Payment;

use Exception;
use Magento\Sales\Model\Order;

class Boleto extends \Magento\Payment\Model\Method\AbstractMethod {

  protected $_code = "boleto";
  protected $_isOffline = true;
  protected $_isInitializeNeeded = false;

  public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null) {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $helperData = $objectManager->create('\Tezus\Asaas\Helper\Data');
    if (!$helperData->getStatusBillet()) {
      return false;
    }
    return parent::isAvailable($quote);
  }

  public function order(\Magento\Payment\Model\InfoInterface $payment, $amount) {
    try {

      //Pegando informações adicionais do pagamento (CPF) (Refatorar)
      $info = $this->getInfoInstance();
      $paymentInfo = $info->getAdditionalInformation();

      //Helper -------- Obs: refatorar
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $helperData = $objectManager->create('\Tezus\Asaas\Helper\Data');
      $days = $helperData->getDays();
      $notification = $helperData->getNotifications();
      //pegando dados do pedido do clioente
      $order = $payment->getOrder();
      $billingaddress = $order->getBillingAddress();
      $shippingaddress = $order->getShippingAddress();
      $shippingAddressExtensionAttributes = $billingaddress->getExtensionAttributes();
      //-----------Pega os dados do usuário necessários para a criação da conta na ASAAS
      $dataUser['name'] = $shippingaddress->getFirstName() . ' ' . $shippingaddress->getLastName();
      $dataUser['email'] = $shippingaddress->getEmail();
      $dataUser['cpfCnpj'] = $paymentInfo['boleto_owner_cpf'];
      if($notification){
        $dataUser['notifications'] = 'false';
      } else {
        $dataUser['notifications'] = 'true';
      }
      if (isset($shippingaddress->getStreet()[1])) {
        $dataUser['addressNumber'] = $shippingaddress->getStreet()[1];
      } else {
        $dataUser['addressNumber'] = 0;
      }

      $dataUser['postalCode'] = $shippingaddress->getPostcode();


      //-----------------Verifica a existência do usuário na Asaaas obs: colocar cpf aqui
      $user = (array)$this->userExists($dataUser['cpfCnpj']);
      if ($user['totalCount']) {
        $currentUser = $user['data'][0]->id;
      } else {
        $newUser = (array)$this->createUser($dataUser);
        $currentUser = $newUser['id'];
      }

      $date = new \DateTime("+$days days");


      //----------Monta os dados para uma cobrança simples com boleto
      $payment['customer'] = $currentUser;
      $payment['value'] = $amount;
      $payment['externalReference'] = $order->getIncrementId();
      $payment['dueDate'] = $date->format('Y-m-d');
      $payment['description'] = "Pedido " . $order->getIncrementId();

      $paymentDone = (array)$this->doPayment($payment);
      $linkBoleto = $paymentDone['bankSlipUrl'];

      //Substituir o uso do object Manager 

      $checkoutSession = $objectManager->create('Magento\Checkout\Model\Session');
      $checkoutSession->setBoleto($linkBoleto);
    } catch (\Exception $e) {
      print_r($e->getMessage());
    }
  }
  public function assignData(\Magento\Framework\DataObject $data) {
    $info = $this->getInfoInstance();
    $info->setAdditionalInformation('boleto_owner_cpf', $data['additional_data']['boleto_owner_cpf'] ?? null);


    return $this;
  }

  public function userExists($cpf) {
    $curl = curl_init();
    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();

    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');

    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/customers?cpfCnpj=" . $cpf,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response);
    //$this->createUser();
  }

  public function createUser($data) {
    $curl = curl_init();
    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();

    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');

    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/customers",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{ 
          "name": "' . $data['name'] . '", 
           "email": "' . $data['email'] . '", 
           "notificationDisabled": "' . $data['notifications'] . '",  
           "cpfCnpj": "' . $data['cpfCnpj'] . '", 
           "postalCode": "' . $data['postalCode'] . '",  
          "addressNumber": "' . $data['addressNumber'] . '" }"',
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response);
  }
  public function doPayment($data) {
    $curl = curl_init();

    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();

    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');
    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/payments",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '
        {"customer": "' . $data['customer'] . '",
        "billingType": "BOLETO",
          "dueDate": "' . $data['dueDate'] . '", 
          "value": "' . $data['value'] . '",
          "externalReference": "' . $data['externalReference'] . '",
          "description": "' . $data['description'] . '"
         
            }',
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    return json_decode($response);
  }
}
