<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Tezus\Asaas\Model\Payment;

use Magento\Sales\Model\Order;
use Magento\TestFramework\ObjectManager;

class Cc extends \Magento\Payment\Model\Method\AbstractMethod {

  protected $_code = "cc";

  protected $_isGateway                   = true;
  protected $_canAuthorize                = false;
  protected $_canCapture                  = true;
  protected $_canCapturePartial           = true;
  protected $_canRefund                   = true;
  protected $_canRefundInvoicePartial     = true;

  public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null) {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $helperData = $objectManager->create('\Tezus\Asaas\Helper\Data');
    if (!$helperData->getStatusCc()) {
      return false;
    }
    return parent::isAvailable($quote);
  }

  public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount) {
    try {
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $date = new \DateTime();
      $helperData = $objectManager->create('\Tezus\Asaas\Helper\Data');
      $notification = $helperData->getNotifications();

      //Info do CC
      $info = $this->getInfoInstance();
      $paymentInfo = $info->getAdditionalInformation();

      //pegando dados do pedido do clioente
      $order = $payment->getOrder();
      $shippingaddress = $order->getShippingAddress();

      //-----------Pega os dados do usuário necessários para a criação da conta na ASAAS
      $dataUser['name'] = $shippingaddress->getFirstName() . ' ' . $shippingaddress->getLastName();
      $dataUser['email'] = $shippingaddress->getEmail();
      $dataUser['cpfCnpj'] = $paymentInfo['cc_owner_cpf'];
      if ($notification) {
        $dataUser['notifications'] = 'false';
      } else {
        $dataUser['notifications'] = 'true';
      }

      if (isset($shippingaddress->getStreet()[1])) {
        $dataUser['addressNumber'] = $shippingaddress->getStreet()[1];
      } else {
        $dataUser['addressNumber'] = 0;
      }

      $dataUser['postalCode'] = $shippingaddress->getPostcode();

      //-----------------Verifica a existência do usuário na Asaaas obs: colocar cpf aqui
      $user = (array)$this->userExists($dataUser['cpfCnpj']);
      if ($user['totalCount']) {
        $currentUser = $user['data'][0]->id;
      } else {
        $newUser = (array)$this->createUser($dataUser);
        $currentUser = $newUser['id'];
      }

      $values = explode("-", $paymentInfo['installments']);

      //build array of payment data for API request.
      $request = [
        'customer' => $currentUser,
        'billingType' => 'CREDIT_CARD',
        'installmentCount' => (int)$values[0],
        'installmentValue' => (float)$values[1],
        'dueDate' => $date->format('Y-m-d'),
        'description' => "Pedido " . $order->getIncrementId(),
        'externalReference' => $order->getIncrementId(),
        'creditCard' => [
          'holderName' => $paymentInfo['credit_card_owner'],
          'number' => $paymentInfo['credit_card_number'],
          'expiryMonth' => $paymentInfo['credit_card_exp_month'],
          'expiryYear' => $paymentInfo['credit_card_exp_year'],
          'ccv' => $paymentInfo['credit_card_cid'],
        ],
        'creditCardHolderInfo' => [
          'name' => $shippingaddress->getFirstName() . ' ' . $shippingaddress->getLastName(),
          'email' => $shippingaddress->getEmail(),
          'cpfCnpj' => $dataUser['cpfCnpj'],
          'postalCode' => $shippingaddress->getPostcode(),
          'addressNumber' => $shippingaddress->getStreet()[1],
          'addressComplement' => null,
          'phone' => $shippingaddress->getTelephone(),
          'mobilePhone' => $paymentInfo['credit_card_phone'],
        ],
      ];

      $paymentDone = (array)$this->doPayment($request);
      if (isset($paymentDone['errors'])) {
        throw new \Exception($paymentDone['errors'][0]->description, 1);
      } else {
        $_canAuthorize = TRUE;
        $linkBoleto = $paymentDone['invoiceUrl'];

        //Substituir o uso do object Manager 

        $checkoutSession = $objectManager->create('Magento\Checkout\Model\Session');
        $checkoutSession->setBoleto($linkBoleto);
        return $this;
      }
    } catch (\Exception $e) {
      $objectManager->create('\Magento\Framework\Message\ManagerInterface')->addErrorMessage('Teste');
      throw new \Magento\Framework\Exception\LocalizedException(__('The authorize action is not available.'));
      return $this;
    }
  }

  public function userExists($cpf) {
    $curl = curl_init();
    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();

    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');

    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/customers?cpfCnpj=" . $cpf,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response);
  }

  public function createUser($data) {
    $curl = curl_init();

    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');

    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/customers",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{ 
          "name": "' . $data['name'] . '", 
           "email": "' . $data['email'] . '", 
           "notificationDisabled": "' . $data['notifications'] . '", 
           "cpfCnpj": "' . $data['cpfCnpj'] . '",  
           "postalCode": "' . $data['postalCode'] . '",  
          "addressNumber": "' . $data['addressNumber'] . '" }"',
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response);
  }

  public function doPayment($data) {
    $curl = curl_init();

    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
    $_decrypt = $object_manager->create('\Magento\Framework\Encryption\EncryptorInterface');
    $helper_factory = $object_manager->create('\Tezus\Asaas\Helper\Data');

    curl_setopt_array($curl, array(
      CURLOPT_URL => $helper_factory->getUrl() . "/api/v3/payments",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "access_token: " . $_decrypt->decrypt($helper_factory->getAcessToken()),
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    return json_decode($response);
  }

  public function assignData(\Magento\Framework\DataObject $data) {
    $info = $this->getInfoInstance();
    $info->setAdditionalInformation('cc_owner_cpf', $data['additional_data']['cc_owner_cpf'] ?? null)
      ->setAdditionalInformation('credit_card_type', $data['additional_data']['cc_type'] ?? null)
      ->setAdditionalInformation('credit_card_cid', $data['additional_data']['cc_cid'] ?? null)
      ->setAdditionalInformation('installments', $data['additional_data']['cc_installments'] ?? null)
      ->setAdditionalInformation('credit_card_number', $data['additional_data']['cc_number'] ?? null)
      ->setAdditionalInformation('credit_card_exp_year', $data['additional_data']['cc_exp_year'] ?? null)
      ->setAdditionalInformation('credit_card_exp_month', $data['additional_data']['cc_exp_month'] ?? null)
      ->setAdditionalInformation('credit_card_phone', $data['additional_data']['cc_phone'] ?? null)
      ->setAdditionalInformation('credit_card_owner', $data['additional_data']['cc_owner_name'] ?? null);

    return $this;
  }
}
