<?php

namespace Tezus\Asaas\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class AdditionalConfigProvider implements ConfigProviderInterface {

  public function __construct(
    \Tezus\Asaas\Helper\Data $helper,
    \Magento\Checkout\Model\Cart $cart,
    \Magento\Payment\Model\CcConfig $ccConfig
    ) {
    $this->helperData = $helper;
    $this->cart = $cart;
    $this->ccConfig = $ccConfig;
  }
  
  public function getInstallments() {
    $installments = $this->helperData->getInstallments();
    return $installments;
  }

  public function getGrandTotal(){
    return $this->cart->getQuote()->getGrandTotal();
  }

  public function getMinParcelas(){
    return $this->helperData->getMinParcela();
  }

  public function getConfig() {
    $config = [
      'payment' => [
        'cc' => [
          'installments' => $this->getInstallments(),
          'grand_total' => $this->getGrandTotal(),
          'min_parcela' => $this->getMinParcelas(),
          'hasVerification' => $this->ccConfig->hasVerification(),
          'cc_types' => $this->helperData->getConfig('payment/tezusasaas/options_cc/cctypes')
        ],
      ],
    ];
    return $config;
  }
}
