<?php
namespace Tezus\Asaas\Api;
//use Tezus\Asaas\Api\Data\PointInterface;
/**
 * @api
 */
interface UpdateStatusesInterface
{
   /**
     * Post Company.
     *
     * @api
     * @param  mixed $event
     * @param  mixed $payment
     * @return  mixed
     */
    public function doUpdate($event,$payment);
}